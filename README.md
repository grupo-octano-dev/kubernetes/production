# Staging Environment

## Namespaces

- staging
- staging-storage
- staging-database

## Ingress

- Not defined yet

## Services

- redis-svc.staging-database.svc.cluster.local

## Deployments/Statefulset

- redis-sf

## PVCs

- Not defined yet

## Storage

- Not defined yet
